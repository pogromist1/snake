const Direction = {
    LEFT: 0,
    RIGHT: 2,
    UP: 3,
    DOWN: 1
};
// 0 влево
// 1 - вниз
// 2 - вправо
// 3 - вверх

const EnumState = {
    LOADED: 1,
    STARTED: 2,
    GAME_OVER: 3,
    PAUSED:4
}