class Snake {
    direction = 0;
    body = [];

    initSnake(x, y, direction, snake_length) {
        this.direction = direction
        switch (this.direction) {
            case Direction.LEFT:
                for (let i = 0; i < snake_length; i++) {
                    this.body.push(new Cell(x + i, y))
                }
                break;
            case Direction.RIGHT:
                for (let i = 0; i < snake_length; i++) {
                    this.body.push(new Cell(x - i, y))
                }
                break;
            case Direction.UP:
                for (let i = 0; i < snake_length; i++) {
                    this.body.push(new Cell(x, y+i));
                }
                break;
            case Direction.DOWN:
                for (let i = 0; i < snake_length; i++) {
                    this.body.push(new Cell(x, y-i));
                }
                break;
        }
    }

    draw(table) {
        this.body.forEach( cell=>
            table.getElementsByTagName("tr")[cell.y].cells[cell.x].bgColor = "red"
        )
    }
    getNewMove(x,y){
        switch(this.direction) {
            case Direction.LEFT:
                return (new Cell(x - 1 , y));
            case Direction.RIGHT:
                return (new Cell(x + 1 , y));
            case Direction.UP:
                return (new Cell(x , y-1));
            case Direction.DOWN:
                return (new Cell(x ,y + 1));
        }
    }

}