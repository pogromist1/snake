let apple;
let intervalId;
let last_direction = -1;
let width = 50;
let height = 25;
let snake;
let snake_length = 5;
let table;
let gameState;

window.onload = initGame;

function initGame() {
    gameState = EnumState.LOADED;

    drawTable();
    initSnake();
    initApple();
}

function drawTable() {
    table = document.createElement('table');
    table.id = "gameField";
    table.align = "left"

    for (let i = 0; i < height; i++) {
        let tr = document.createElement("tr")
        for (let i = 0; i < width; i++) {
            tr.appendChild(document.createElement("td"))
        }
        table.appendChild(tr)
    }

    document.getElementById('body').replaceChild(table,document.getElementById("gameField"));
}

function initSnake() {
    let x = Math.floor((width - snake_length) / 2);
    let y = Math.floor(height / 2);
    snake = new Snake();
    snake.initSnake(x, y, Direction.RIGHT, snake_length);
    snake.draw(table)
}

function initApple() {
    let random_x = Math.floor(Math.random() * width);
    let random_y = Math.floor(Math.random() * height)
    apple = new Apple(random_x, random_y);
    apple.draw(table)
}

function startGame() {
    gameState = EnumState.STARTED;
    intervalId = setInterval(moveAndDrawSnake, 100);
}

function checkCollisions(newFirst) {
    if (snake.body.filter(i => i.x === newFirst.x && i.y === newFirst.y).length > 0) {
        return true;
    } else if (newFirst.x >= width) {
        return true;
    }
    if (newFirst.x < 0) {
        return true;
    }
    if (newFirst.y >= height) {
        return true;
    }
    return newFirst.y < 0;
}

function moveAndDrawSnake() {
    let tail = snake.body[snake.body.length - 1];
    let first = snake.body[0]
    window.onKeyDown = null;
    document.getElementById("snakeLengthLabel").textContent = "Длина змейки: " + snake.body.length
    let newFirst;

    newFirst = snake.getNewMove(first.x, first.y)

    if (checkCollisions(newFirst)) {
        gameOver()
    } else {
        if (snake.body.filter(i => i.x === apple.x && i.y === apple.y).length > 0) {
            let random_x = Math.floor(Math.random() * width);
            let random_y = Math.floor(Math.random() * height)
            apple.x = random_x;
            apple.y = random_y;

        } else {
            snake.body.pop()
        }
    }
    last_direction = snake.direction;
    snake.body = [newFirst].concat(snake.body);

    table.getElementsByTagName("tr")[tail.y].cells[tail.x].bgColor = "white";
    apple.draw(table)
    snake.draw(table)

    console.log(String(gameState))
}

function pauseGame() {
    gameState = EnumState.PAUSED
    clearInterval(intervalId);
}

function restartGame() {
    initGame()
}

function resumeGame() {
    gameState = EnumState.STARTED
    intervalId = setInterval(moveAndDrawSnake, 100);
}

function gameOver() {
    gameState = EnumState.GAME_OVER
    clearInterval(intervalId);
    alert("Game Over")
}

window.onkeydown = function (e) {
    let k = e.keyCode;
    if ([38, 39, 40, 37, 13].indexOf(k) >= 0) e.preventDefault();
    if (k === 39 && last_direction !== Direction.LEFT) snake.direction = Direction.RIGHT; //Вправо
    if (k === 40 && last_direction !== Direction.UP) snake.direction = Direction.DOWN //Вниз
    if (k === 37 && last_direction !== Direction.RIGHT) snake.direction = Direction.LEFT; //Влево
    if (k === 38 && last_direction !== Direction.DOWN) snake.direction = Direction.UP //Вверх

    if (k === 13) {
        //TODO переделать на switch
        if (gameState === EnumState.LOADED) {
            console.log("w1")
            startGame();
        } else if (gameState === EnumState.STARTED) {
            console.log("w2")
            pauseGame()
        } else if (gameState === EnumState.GAME_OVER) {
            console.log("w3")
            restartGame()
        } else if (gameState === EnumState.PAUSED) {
            console.log("w4")
            resumeGame()
        }
    }
}